package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entities.History;
import com.example.demo.entities.StockOrder;
import com.example.demo.repository.StockOrderInterface;

@Service
public class StockOrderService {

	@Autowired
	private StockOrderInterface repository;

	public List<StockOrder> getAllOrders() {
		return repository.getAllOrders();
	}

	public StockOrder getOrder(int id) {
		return repository.getOrderById(id);
	}
	
	public StockOrder getOrderByTicker(String stockTicker) {
		return repository.getOrderByTicker(stockTicker);
	}

	public StockOrder editOrder(StockOrder order) {
		return repository.editOrder(order);
	}

	public StockOrder addOrder(StockOrder order) {
		return repository.addOrder(order);
	}

	public int deleteOrder(int id) {
		return repository.deleteOrder(id);
	}
	
	public List<History> getHistory() {
		return repository.getHistory();
	}
	public List<StockOrder> getMergeOrders() {
		return repository.getMergeOrders();
	}
	
	public List<History> getTop3()
	{
		return repository.getTop3();
	}

}
