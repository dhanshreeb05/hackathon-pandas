package com.example.demo.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.example.demo.entities.History;
import com.example.demo.entities.StockOrder;

@Repository
public class MySQLStockOrdersRepository implements StockOrderInterface {

	@Autowired
	JdbcTemplate template;

	@Override
	public List<StockOrder> getAllOrders() {
		// TODO Auto-generated method stub
		String sql = "SELECT id, stockTicker, stockPrice, volume, buyOrSell, statusCode FROM stock_orders";
		return template.query(sql, new StockOrderRowMapper());

	}

	@Override
	public List<StockOrder> getMergeOrders() {
		// TODO Auto-generated method stub
		String sql = "SELECT id, stockTicker, stockPrice, SUM(volume) as volume , buyOrSell, statusCode FROM stock_orders where buyOrSell='0' GROUP BY stockTicker";
		return template.query(sql, new StockOrderRowMapper());

	}

	@Override
	public StockOrder getOrderById(int id) {
		// TODO Auto-generated method stub
		String sql = "SELECT id, stockTicker, stockPrice, volume, buyOrSell, statusCode FROM stock_orders WHERE id=?";
		return template.queryForObject(sql, new StockOrderRowMapper(), id);
	}

	@Override
	public StockOrder getOrderByTicker(String stockTicker) {
		// TODO Auto-generated method stub
		String sql = "SELECT id, stockTicker, stockPrice, volume, buyOrSell, statusCode FROM stock_orders WHERE stockTicker=?";
		return template.queryForObject(sql, new StockOrderRowMapper(), stockTicker);
	}

	@Override
	public StockOrder editOrder(StockOrder stockOrder) {
		// TODO Auto-generated method stub
		System.out.println(stockOrder.getStockTicker());
		System.out.println(stockOrder.getId());
		System.out.println(stockOrder.getStockPrice());
		System.out.println(stockOrder.getVolume());
		System.out.println("----Hello----");
		String sql = "UPDATE stock_orders SET stockTicker=?, stockPrice=?, volume=?, buyOrSell=?, statusCode=? WHERE id = ?";
		template.update(sql, stockOrder.getStockTicker(), stockOrder.getStockPrice(), stockOrder.getVolume(), "0",
				stockOrder.getStatusCode(), stockOrder.getId());
		java.util.Date utilDate = new java.util.Date();
		String sql1 = "INSERT INTO history(stockTicker, stockPrice, volume, buyOrSell, statusCode, dateTime) "
				+ "VALUES(?,?,?,?,?,?)";
		java.sql.Timestamp dateTime = new java.sql.Timestamp(utilDate.getTime());
		String dateTime1 = dateTime.toString();
		dateTime1 = dateTime1.substring(0, dateTime1.indexOf('.'));

		template.update(sql1, stockOrder.getStockTicker(), stockOrder.getStockPrice(),
				Integer.parseInt(stockOrder.getBuyOrSell()), "sell", stockOrder.getStatusCode(), dateTime1);

		return stockOrder;
	}

	@Override
	public int deleteOrder(int id) {
		StockOrder ss=getOrderById(id);
		java.util.Date utilDate = new java.util.Date();
		String sql1 = "INSERT INTO history(stockTicker, stockPrice, volume, buyOrSell, statusCode, dateTime) "
				+ "VALUES(?,?,?,?,?,?)";
		
		java.sql.Timestamp dateTime = new java.sql.Timestamp(utilDate.getTime());
		String dateTime1 = dateTime.toString();
		dateTime1 = dateTime1.substring(0, dateTime1.indexOf('.'));

		template.update(sql1, ss.getStockTicker(), ss.getStockPrice(),ss.getVolume() , "sell", ss.getStatusCode(), dateTime1);
		String sql = "DELETE FROM stock_orders WHERE id = ?";
		template.update(sql, id);
		return id;

	}

	@Override
	public StockOrder addOrder(StockOrder stockOrder) {
		// TODO Auto-generated method stub
		String sql = "INSERT INTO stock_orders(stockTicker, stockPrice, volume, buyOrSell, statusCode) "
				+ "VALUES(?,?,?,?,?)";
		template.update(sql, stockOrder.getStockTicker(), stockOrder.getStockPrice(), stockOrder.getVolume(),
				stockOrder.getBuyOrSell(), stockOrder.getStatusCode());

		java.util.Date utilDate = new java.util.Date();
		String sql1 = "INSERT INTO history(stockTicker, stockPrice, volume, buyOrSell, statusCode, dateTime) "
				+ "VALUES(?,?,?,?,?,?)";
		java.sql.Timestamp dateTime = new java.sql.Timestamp(utilDate.getTime());
		String dateTime1 = dateTime.toString();
		dateTime1 = dateTime1.substring(0, dateTime1.indexOf('.'));

		template.update(sql1, stockOrder.getStockTicker(), stockOrder.getStockPrice(), stockOrder.getVolume(), "buy",
				stockOrder.getStatusCode(), dateTime1);
		return stockOrder;
	}

	@Override
	public List<History> getHistory() {

		// TODO Auto-generated method stub
		String sql = "SELECT * FROM history";
		return template.query(sql, new HistoryRowMapper());

	}

	@Override
	public List<History> getTop3() {
		// TODO Auto-generated method stub
		String sql = "select *, count(*) as freqency from history group by stockTicker order by count(*) desc limit 3";

		return template.query(sql, new HistoryRowMapper());
	}

}

class StockOrderRowMapper implements RowMapper<StockOrder> {

	@Override
	public StockOrder mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new StockOrder(rs.getInt("id"), rs.getString("stockTicker"), rs.getDouble("stockPrice"),
				rs.getInt("volume"), rs.getString("buyOrSell"), rs.getInt("statusCode"));
	}

}

class HistoryRowMapper implements RowMapper<History> {

	@Override
	public History mapRow(ResultSet rs, int rowNum) throws SQLException {
		// System.out.println(rs.getTimestamp("dateTime"));
		return new History(rs.getInt("id"), rs.getString("stockTicker"), rs.getDouble("stockPrice"),
				rs.getInt("volume"), rs.getString("buyOrSell"), rs.getInt("statusCode"), rs.getString("dateTime"));
	}

}