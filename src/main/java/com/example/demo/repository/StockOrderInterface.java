package com.example.demo.repository;

import java.util.List;

import org.springframework.stereotype.Component;

import com.example.demo.entities.History;
import com.example.demo.entities.StockOrder;

@Component
public interface StockOrderInterface {

	public List<StockOrder> getAllOrders();

	public StockOrder getOrderById(int id);
	
	public StockOrder getOrderByTicker(String stockTicker);

	public StockOrder editOrder(StockOrder stockOrder);

	public int deleteOrder(int id);

	public StockOrder addOrder(StockOrder stockOrder);

	public List<History> getHistory();
	
	public List<StockOrder> getMergeOrders();
	
	public List<History> getTop3();

}
