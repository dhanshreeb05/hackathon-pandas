package com.example.demo.entities;

public class StockOrder {

	private int id;
	private String stockTicker;
	private double stockPrice;
	private int volume;
	private String buyOrSell;
	private int statusCode;

	public StockOrder() {

	}

	public StockOrder(int id, String stockTicker, double stockPrice, int volume, String buyOrSell, int statusCode) {
		super();
		this.id = id;
		this.stockTicker = stockTicker;
		this.stockPrice = stockPrice;
		this.volume = volume;
		this.buyOrSell = buyOrSell;
		this.statusCode = statusCode;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStockTicker() {
		return stockTicker;
	}

	public void setStockTicker(String stockTicker) {
		this.stockTicker = stockTicker;
	}

	public double getStockPrice() {
		return stockPrice;
	}

	public void setStockPrice(double stockPrice) {
		this.stockPrice = stockPrice;
	}

	public int getVolume() {
		return volume;
	}

	public void setVolume(int volume) {
		this.volume = volume;
	}

	public String getBuyOrSell() {
		return buyOrSell;
	}

	public void setBuyOrSell(String buyOrSell) {
		this.buyOrSell = buyOrSell;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
}
