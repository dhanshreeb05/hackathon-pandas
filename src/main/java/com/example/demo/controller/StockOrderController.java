package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entities.History;
import com.example.demo.entities.StockOrder;
import com.example.demo.service.StockOrderService;

@RestController
@CrossOrigin("*")

@RequestMapping("api/stockOrder")//root end point of API
public class StockOrderController {
	
	@Autowired
	StockOrderService service;
	
	@GetMapping(value = "/")//getallorders
	public List<StockOrder> getAllStockOrders() {
		return service.getAllOrders();
	}
	@GetMapping(value = "/merge")//getallorders
	public List<StockOrder> getMergeStockOrders() {
		return service.getMergeOrders();
	}

	@GetMapping(value = "/{id}")
	public StockOrder getStockOrderById(@PathVariable("id") int id) {
	  return service.getOrder(id);
	}
	@GetMapping(value = "/search/{stockTicker}")
	public StockOrder getOrderByTicker(@PathVariable("stockTicker") String stockTicker) {
	  return service.getOrderByTicker(stockTicker);
	}

	@PostMapping(value = "/")
	public StockOrder addStockOrder(@RequestBody StockOrder stockOrder) {
		return service.addOrder(stockOrder);
	}

	@PutMapping(value = "/")
	public StockOrder editOrder(@RequestBody StockOrder stockOrder) {
		return service.editOrder(stockOrder);
	}

	@DeleteMapping(value = "/{id}")
	public int deleteStockOrder(@PathVariable int id) {
		return service.deleteOrder(id);
	}
	
	@GetMapping(value = "/getHistory")//getallorders
	public List<History> getHistory() {
		return service.getHistory();
	}
	
	@GetMapping(value = "/getTop3")//getallorders
	public List<History> getTop3() {
		return service.getTop3();
	}

}