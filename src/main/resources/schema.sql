
CREATE TABLE IF NOT EXISTS stock_orders (

  id int NOT NULL AUTO_INCREMENT,
  stockTicker varchar(45) NOT NULL,
  stockPrice double NOT NULL,
  volume int NOT NULL,
  buyOrSell varchar(45)	 DEFAULT NULL,
  statusCode int NOT NULL DEFAULT 0,
  PRIMARY KEY (id)
);

INSERT INTO stock_orders (stockTicker, stockPrice, volume, buyOrSell, statusCode)  VALUES('FB',340.76,3,'0',0);
INSERT INTO stock_orders (stockTicker, stockPrice, volume, buyOrSell, statusCode)  VALUES('NFLZ',260.76,5,'0',0);
INSERT INTO stock_orders (stockTicker, stockPrice, volume, buyOrSell, statusCode)  VALUES('IBM',120.34,8,'0',0);
