package com.example.demo.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import com.example.demo.entities.StockOrder;
import com.example.demo.repository.StockOrderInterface;

@ActiveProfiles("h2")
@SpringBootTest

public class StockOrderServiceTest {
	// Get a "real" shipperService from the spring container
		@Autowired
		StockOrderService stockService;

		// Get a "mock" or "fake" shipper repository
		// from the spring container
		@MockBean
		StockOrderInterface stockRepository;
		
		@Test
		public void testGetStock() {
			//1. Setup stuff
			// create a shipper record for testing
			int testId = 7;
			String testName = "testStocks";
			StockOrder testStock = new StockOrder();
			testStock.setId(testId);
			testStock.setStockTicker(testName);
			
			// tell the mock object what to do when
			// its getShipperById method is called
			when(stockRepository.getOrderById(testStock.getId()))
			.thenReturn(testStock);
			
			
			//2. Class class under test
			// call the getShipper method on the service, this will call
			// the getShipperById method on the the mock repository
			// the mock repository returns the testShipper
			// the service should return the same testShipper here
			// we verify this happens, so we know the service is behaving as expected
			StockOrder returnedStock = stockService.getOrder(testId);
			
			//3. verify response
			assertThat(returnedStock).isEqualTo(testStock);
			//assertThat(shipperService.getShipper(testId)).isEqualTo(testShipper);
		}
	}

	//line 57 extra...and comment 60 and add 61