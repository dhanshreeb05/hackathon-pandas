package com.example.demo.repository;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import com.example.demo.entities.StockOrder;

@ActiveProfiles("h2")
@SpringBootTest
public class MySQLStockOrdersRepositoryTest {
	@Autowired
	MySQLStockOrdersRepository mySQLStockOrderRepository;
	
	@Test
	public void testGetAllShippers() {
		List<StockOrder> returnedList = mySQLStockOrderRepository.getAllOrders();
		assert(returnedList != null);
		
		for(StockOrder stocks: returnedList) {
			System.out.println("Ticker name: "+ stocks.getStockTicker());
		}
	}
}

