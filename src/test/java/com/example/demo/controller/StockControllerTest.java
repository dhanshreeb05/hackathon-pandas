package com.example.demo.controller;


import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.example.demo.entities.StockOrder;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@ActiveProfiles("h2")
@SpringBootTest
@AutoConfigureMockMvc
public class StockControllerTest {
	@Autowired
	private MockMvc mockMvc;

	@Test
	public void testShouldReturnListSize3() throws Exception {
		// 1. setup stuff
		//2. call method under test
		MvcResult mvcResult = this.mockMvc.perform(get("/api/stockOrder/"))
							              .andDo(print())
							              .andExpect(status().isOk())
							              .andReturn();
		//3. verify the results
		List<StockOrder> stocks = new ObjectMapper().readValue(
											mvcResult.getResponse().getContentAsString(),
				                            new TypeReference<List<StockOrder>>() { });
		
		assertThat(stocks.size()).isGreaterThan(0);
	}
	
	@Test
	public void testFindByIDSuccess() throws Exception {
		// 1. setup stuff
		//2. call method under test
		MvcResult mvcResult = this.mockMvc.perform(get("/api/stockOrder/1"))
							              .andDo(print())
							              .andExpect(status().isOk())
							              .andReturn();
		//3. verify the results
		StockOrder stocks = new ObjectMapper().readValue(
											mvcResult.getResponse().getContentAsString(),
				                            new TypeReference<StockOrder>() { });
		
		assertThat(stocks.getId()).isEqualTo(1);
	}
	
	@Test
	public void testFindByIDFailure() throws Exception {
		// 1. setup stuff
		//2. call method under test
		this.mockMvc.perform(get("/api/stockOrder/a"))
							              .andDo(print())
							              .andExpect(status().isBadRequest())
							              .andReturn();
	}
	
}

