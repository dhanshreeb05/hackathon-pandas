
CREATE TABLE IF NOT EXISTS stock_orders (

  id int NOT NULL AUTO_INCREMENT,
  stockTicker varchar(45) NOT NULL,
  stockPrice double NOT NULL,
  volume int NOT NULL,
  buyOrSell varchar(45)	 DEFAULT NULL,
  statusCode int NOT NULL DEFAULT 0,
  PRIMARY KEY (id)
);
